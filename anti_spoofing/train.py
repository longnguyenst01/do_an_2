import datetime
from models import *
import tensorflow as tf
from prepare_data import Data_generator
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.callbacks import EarlyStopping

batch_size = 16
data_train = Data_generator(batch_size, "train")
data_val = Data_generator(batch_size,"test")
model = resnet_18()

weights_path = "weight/model_18.hdf5"
model.load_weights(weights_path)

losses = ["mse","binary_crossentropy",
          "categorical_crossentropy","categorical_crossentropy",
          "categorical_crossentropy"]

log_dir = "logs/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)
callbacks = [
        EarlyStopping(
            monitor="val_loss", min_delta=0, patience=8, restore_best_weights=False
        ),
        ModelCheckpoint(
            weights_path, monitor="val_loss", save_best_only=True
        ),
        tensorboard_callback
    ]

lossWeights = [1, 1.0, 0.1, 0.01, 1.0]
model.compile(optimizer="adam", loss=losses, loss_weights=lossWeights,
              metrics=["acc"])
model.fit_generator(generator= data_train, steps_per_epoch=int(240000/batch_size),epochs = 10
                    ,validation_data=data_val,validation_steps=int(25000/batch_size),  callbacks=callbacks)

