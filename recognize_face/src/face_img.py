
import sys
from model import resnet_18
import tensorflow as tf
import argparse
import facenet
import os
import sys
import math
import pickle
import align.detect_face
import numpy as np
import cv2
from tensorflow import convert_to_tensor as tensor

import collections
from sklearn.svm import SVC
def img_to_tensor(img):
    faceImg = []
    img = cv2.resize(img, (256, 256)) - np.array([107, 122, 151])
    img = tensor(img / 255)
    faceImg.append(img)
    return tensor(faceImg)

class recognize_face():
    def __init__(self):
        self.MINSIZE = 20
        self.THRESHOLD = [0.6, 0.7, 0.7]
        self.FACTOR = 0.709
        self.IMAGE_SIZE = 182
        self.INPUT_IMAGE_SIZE = 160
        self.CLASSIFIER_PATH = 'Models/facemodel.pkl'
        self.FACENET_MODEL_PATH = 'Models/20180402-114759.pb'
        self.path_face_anti_spoofing = "../anti_spoofing/model_18.hdf5"
        self.model_face_anti_spoofing = resnet_18()
        self.model_face_anti_spoofing.load_weights(self.path_face_anti_spoofing)
        with open(self.CLASSIFIER_PATH, 'rb') as file:
            self.model, self.class_names = pickle.load(file)
        print("Custom Classifier, Successfully loaded")
        with tf.Graph().as_default():
            # Cai dat GPU neu co
            gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.6)
            self.sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options, log_device_placement=False))

            with self.sess.as_default():
                # Load model MTCNN phat hien khuon mat
                print('Loading feature extraction model')
                facenet.load_model(self.FACENET_MODEL_PATH)
                self.facenet = facenet
                # Lay tensor input va output
                self.images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
                self.embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
                self.phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")
                self.embedding_size = self.embeddings.get_shape()[1]
                # Cai dat cac mang con
                self.pnet, self.rnet, self.onet = align.detect_face.create_mtcnn(self.sess, "src/align")


    def run(self, img):

        bounding_boxes, _ = align.detect_face.detect_face(img, self.MINSIZE, self.pnet, self.rnet, self.onet, self.THRESHOLD, self.FACTOR)

        faces_found = bounding_boxes.shape[0]

        try:
            # Neu co it nhat 1 khuon mat trong frame
            if faces_found > 0:
                det = bounding_boxes[:, 0:4]
                bb = np.zeros((faces_found, 4), dtype=np.int32)
                for i in range(faces_found):
                    bb[i][0] = det[i][0]
                    bb[i][1] = det[i][1]
                    bb[i][2] = det[i][2]
                    bb[i][3] = det[i][3]

                    # Cat phan khuon mat tim duoc
                    cropped = img[bb[i][1]:bb[i][3], bb[i][0]:bb[i][2], :]
                    # cv2.imshow("img", cropped)
                    # cv2.waitKey(0)
                    scaled = cv2.resize(cropped, (self.INPUT_IMAGE_SIZE, self.INPUT_IMAGE_SIZE),
                                        interpolation=cv2.INTER_CUBIC)
                    scaled = facenet.prewhiten(scaled)
                    scaled_reshape = scaled.reshape(-1, self.INPUT_IMAGE_SIZE, self.INPUT_IMAGE_SIZE, 3)
                    feed_dict = {self.images_placeholder: scaled_reshape, self.phase_train_placeholder: False}
                    emb_array = self.sess.run(self.embeddings, feed_dict=feed_dict)

                    # Dua vao model de classifier
                    predictions = self.model.predict_proba(emb_array)
                    best_class_indices = np.argmax(predictions, axis=1)
                    best_class_probabilities = predictions[
                        np.arange(len(best_class_indices)), best_class_indices]

                    # Lay ra ten va ty le % cua class co ty le cao nhat
                    best_name = self.class_names[best_class_indices[0]]
                    print("Name: {}, Probability: {}".format(best_name, best_class_probabilities))

                    # Ve khung mau xanh quanh khuon mat
                    cv2.rectangle(img, (bb[i][0], bb[i][1]), (bb[i][2], bb[i][3]), (0, 255, 0), 2)
                    text_x = bb[i][0]
                    text_y = bb[i][3] + 20

                    # Neu ty le nhan dang > 0.5 thi hien thi ten
                    if best_class_probabilities > 0.8:
                        name = self.class_names[best_class_indices[0]]
                    else:
                        # Con neu <=0.5 thi hien thi Unknow
                        name = "Unknown"
                    result = self.model_face_anti_spoofing.predict(x=img_to_tensor(img), steps=1)
                    fake = result[-1][-1][-1]
                    print("fake:{}".format(round(float(fake),3)))
                    # Viet text len tren frame
                    cv2.putText(img, "fake:{}".format(round(float(fake),3)), (text_x, text_y-34), cv2.FONT_HERSHEY_COMPLEX_SMALL,
                                1, (255, 0, 255), thickness=1, lineType=2)
                    cv2.putText(img, name + ":" + str(round(best_class_probabilities[0], 3)), (text_x, text_y), cv2.FONT_HERSHEY_COMPLEX_SMALL,
                                1, (255, 255, 0), thickness=1, lineType=2)
                    # cv2.putText(img, str(round(best_class_probabilities[0], 3)), (text_x, text_y + 17),
                    #             cv2.FONT_HERSHEY_COMPLEX_SMALL,
                    #             1, (255, 255, 0), thickness=1, lineType=2)

        except:
            pass

            # Hien thi frame len man hinh
        cv2.imshow('Face Recognition', img)
        cv2.waitKey(0)



path = "/home/longnguyen/pycharm/do_an_2/test_img"
dirs = os.listdir(path)
recognize = recognize_face()
for dir in dirs:
    img = cv2.imread(os.path.join(path, dir))
    recognize.run(img)
    # run(img)
