import cv2
import json
import os.path
from FT_image import FT
import os
root = "/home/longnguyen/Downloads/celebA/CelebA_Spoof_/CelebA_Spoof"
def get_file(option):
    if option == "train":
        name = os.path.join(root, "metas/protocol1/train_label.json")
    if option == "test":
        name = os.path.join(root, "metas/protocol1/test_label.json")
    read_file = open(name).read()
    file = json.loads(read_file)
    return file


def getPathValue(option):
    file1 = get_file(option)
    path = []
    value = []
    for each_key, each_value in file1.items():
        path.append(each_key)
        value.append(each_value)
    return path, value

def get_box(path, option):
    if option == "train":
        path_img = path[:-7] + ".jpg"
    if option == "test":
        path_img = path[:-7] + ".png"
    img = cv2.imread(path_img)
    real_h, real_w, _ = img.shape
    file = open(path, "r")
    line = file.readline()
    split =  line.split(" ")
    bbox = [int(split[0]),int(split[1]),int(split[2]),int(split[3])]
    x1 = int(bbox[0] * (real_w / 224))
    y1 = int(bbox[1] * (real_h / 224))
    w1 = int(bbox[2] * (real_w / 224))
    h1 = int(bbox[3] * (real_h / 224))
    box = [x1, y1, x1 + w1, y1 +h1]
    return box
def crop(option):
    all_path, value = getPathValue(option)
    for path in all_path[:]:

        box = get_box(os.path.join(root, path[:-4] + '_BB.txt'), option)
        image_iter = cv2.imread(os.path.join(root, path))
        h, w, _ = image_iter.shape
        print(path.split("/")[-1])

        face_img_iter = image_iter[max(box[1], 0):min(box[3], h), max(box[0], 0):min(box[2], w)]
        if "000820.jpg" in path:
            print(box)
            cv2.imshow("img", face_img_iter)
            cv2.waitKey(0)
        os.remove(os.path.join(root, path))
        # FT_iter = FT(face_img_iter)
        # cv2.imwrite(os.path.join(root,path[:-4]+"_face.jpg"),face_img_iter)
        # cv2.imwrite(os.path.join(root,path[:-4]+"_FT.jpg"),FT_iter)





if __name__ =="__main__":
    crop("test")
