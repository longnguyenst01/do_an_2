import cv2
import os
import uuid
def rename(path):
    dirs = os.listdir(path)
    for dir in dirs:
        img = cv2.imread(os.path.join(path, dir))
        name = uuid.uuid4().hex + ".jpg"
        cv2.imwrite(os.path.join(path, name), img)
        os.remove(os.path.join(path, dir))

if __name__ == "__main__":
    path = "/home/longnguyen/pycharm/do_an_2/recognize_face/Dataset/FaceData/raw/tran_thanh"
    rename(path)