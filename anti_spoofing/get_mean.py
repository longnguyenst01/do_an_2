import cv2
import json
import random
import os.path
import numpy as np

def get_file(option):
    if option == "train":
        name = "/home/longnguyen/Downloads/celebA/CelebA_Spoof_/CelebA_Spoof/metas/protocol1/train_label.json"
    if option == "test":
        name = "/home/longnguyen/Downloads/celebA/CelebA_Spoof_/CelebA_Spoof/metas/protocol1/test_label.json"
    readFile = open(name).read()
    file = json.loads(readFile)
    return file


def getPathValue(option):
    file1 = get_file(option)
    path = []
    value = []
    for eachKey, eachValue in file1.items():
        path.append(eachKey)
        value.append(eachValue)
    return path, value

def get_box(path, option):
    if option == "train":
        path_img = path[:-7] + ".jpg"
    if option == "test":
        path_img = path[:-7] + ".png"
    img = cv2.imread(path_img)
    real_h, real_w, _ = img.shape
    file = open(path, "r")
    line = file.readline()
    split =  line.split(" ")
    bbox = [int(split[0]),int(split[1]),int(split[2]),int(split[3])]
    x1 = int(bbox[0] * (real_w / 224))
    y1 = int(bbox[1] * (real_h / 224))
    w1 = int(bbox[2] * (real_w / 224))
    h1 = int(bbox[3] * (real_h / 224))
    box = [x1, y1, x1 + w1, y1 +h1]
    return box

def Data_generator(batchSize, option):
    path, value = getPathValue(option)
    if option == "train":
        index = [*range(0, len(path), 1)]
        random.shuffle(index)
    if option == "test":
        index = [*range(0, len(path), 1)]
    path = [path[i] for i in index]


    count = -1
    root = "/home/longnguyen/Downloads/celebA/CelebA_Spoof_/CelebA_Spoof"
    while True:

        sum_img = np.zeros(shape=(1,3))
        sum_FT = np.zeros(shape=(1,1))

        LiveLabel = []
        while len(LiveLabel) < batchSize:
            # try:
                count = count + 1

                Image_iter = cv2.imread(os.path.join(root, path[count][:-4] + "_face.jpg"))

                faceImg_iter = cv2.resize(Image_iter, (256, 256))
                a = np.array([[0,0,0]])
                print(a.shape)
                sum_img = sum_img + np.array([[np.mean(faceImg_iter[:,:,0]), np.mean(faceImg_iter[:,:,1]), np.mean(faceImg_iter[:,:,2])]])

                FT_iter = cv2.imread(os.path.join(root, path[count][:-4]+"_FT.jpg"),0)
                FT_iter = cv2.resize(FT_iter, (256, 256))
                sum_FT = sum_FT + np.array(np.mean(FT_iter))
                print("sum_img: {}".format(sum_img))
                print("sum_FT: {}".format(sum_FT))
                print("count: {}".format(count))


if __name__ =="__main__":
    Data_generator(8, "train")
