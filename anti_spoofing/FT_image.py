import cv2
import numpy as np
def FT(img):
    # img = cv2.resize(img, (256,256))
    f = np.fft.fft2(img)
    fshift = np.fft.fftshift(f)
    magnitude_spectrum = (20*np.log(np.abs(fshift))).astype("uint8")
    return magnitude_spectrum

