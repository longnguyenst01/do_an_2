import cv2
from models import *
from tensorflow import convert_to_tensor as tensor
import numpy as np
from prepare_data import getPathValue
import os
def img_to_tensor(img):
    faceImg = []
    img = cv2.resize(img, (256, 256)) - np.array([107, 122, 151])
    img = tensor(img / 255)
    faceImg.append(img)
    return tensor(faceImg)

if __name__ == "__main__":
    model_face_anti_spoofing = resnet_34()
    weights_path = "weight/model_34.hdf5"
    model_face_anti_spoofing.load_weights(weights_path)
    paths, values = getPathValue("test")
    root = "/home/longnguyen/Downloads/celebA/CelebA_Spoof_/CelebA_Spoof"
    model_face_anti_spoofing.summary()
    real_num = 0
    fake_num = 0
    for path in paths:
        print(path)
        img = cv2.imread(os.path.join(root, path[:-4])+"_face.jpg")
        result = model_face_anti_spoofing.predict(x=img_to_tensor(img), steps=1)
        fake =  result[-1][-1][-1]
        print(result[-1][-1])
        print(path.split("/")[3])
        print("---------------")

        if fake > 0.5:
            fake_num = fake_num + 1
        else:
            real_num = real_num + 1
    print("xxxxxxxxxxxxxxxxxxx")
    print("fake: {}".format(fake_num))
    print("real: {}".format(real_num))
    print("xxxxxxxxxxxxxxxxxxx")


        # cv2.imshow("img", img)
        # cv2.waitKey(0)