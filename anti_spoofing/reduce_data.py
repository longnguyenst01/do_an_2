from prepare_data import *
from multiprocessing import Pool
import cv2
import os


def reduce(paths):

    for path in paths:
        img = cv2.imread(path)
        box = get_box(path[:-4] + '_BB.txt', "train")
        img = img[box[1]:box[3], box[0]:box[2]]
        # os.remove(path)
        print(path.split(".")[0]+"cr."+path.split(".")[1], img)
        cv2.imwrite(path.split(".")[0]+"cr."+path.split(".")[1], img)



if __name__ == "__main__":
    root = "/home/longnguyen/data/CelebA_Spoof_/CelebA_Spoof"
    paths, values = getPathValue("train")
    paths = [os.path.join(root, path_iter) for path_iter in paths]
    reduce(paths)
    # split = 4
    # split_data = [paths[i::split] for i in range(split)]
    # p = Pool(split)
    # results = p.map(reduce, split_data)